import { Suspense } from "react"
import { User, Heart, MagnifyingGlass } from "@medusajs/icons"
import { listRegions } from "@lib/data"
import LocalizedClientLink from "@modules/common/components/localized-client-link"
import CartButton from "@modules/layout/components/cart-button"
import SideMenu from "@modules/layout/components/side-menu"
import { FaFacebookF, FaInstagram, FaYoutube } from "react-icons/fa"

export default async function Nav() {
  const regions = await listRegions().then((regions) => regions)

  return (
    <div className="sticky top-0 inset-x-0 z-50 group">
      <header className=" test relative h-16 mx-auto border-b duration-200 bg-white border-ui-border-base">
        <div className="socialmedia flex items-center gap-4 ml-4">
          <a
            href="https://www.facebook.com/pbnailspoland"
            className="socialmedia"
            target="_blank"
          >
            <FaFacebookF />
          </a>
          <a
            href="https://www.instagram.com/pb_nails_poland/"
            className="socialmedia"
            target="_blank"
          >
            <FaInstagram />
          </a>
          <a
            href="https://www.youtube.com/channel/UCxmil_t08kzsBfd1q7LcXjw"
            className="socialmedia"
            target="_blank"
          >
            <FaYoutube />
          </a>
        </div>
        <nav className="content-container txt-xsmall-plus text-ui-fg-subtle flex items-center justify-between w-full h-full text-small-regular">
          <div className="flex-1 basis-0 h-full flex items-center">
            <div className="h-full">
              <SideMenu regions={regions} />
            </div>
          </div>

          <div className=" flex items-center h-full ">
            <LocalizedClientLink
              href="/"
              className="txt-compact-xlarge-plus hover:text-ui-fg-base uppercase"
              data-testid="nav-store-link"
            >
              <img src="https://pbnails.pl/template/rwd/prod/images/logo.svg?ts=1649827295"></img>
            </LocalizedClientLink>
          </div>

          <div className="flex items-center gap-x-6 h-full flex-1 basis-0 justify-end">
            <div className="hidden small:flex items-center gap-x-6 h-full">
              {process.env.FEATURE_SEARCH_ENABLED && (
                <LocalizedClientLink
                  className="hover:text-ui-fg-base"
                  href="/search"
                  scroll={false}
                  data-testid="nav-search-link"
                >
                  Search
                </LocalizedClientLink>
              )}
              <div className="search">
                <form action="/search" method="GET">
                  <input type="text" name="query" placeholder="" />
                  <button type="submit">
                    <MagnifyingGlass />
                  </button>
                </form>
              </div>
              <LocalizedClientLink
                className="  hover:text-ui-fg-base"
                href="/account"
                data-testid="nav-account-link"
              >
                <div className="nav1">
                  <Heart />
                </div>
              </LocalizedClientLink>
              <LocalizedClientLink
                className="  hover:text-ui-fg-base"
                href="/account"
                data-testid="nav-account-link"
              >
                <div className="nav1">
                  <User />
                </div>
              </LocalizedClientLink>
            </div>
            <Suspense
              fallback={
                <LocalizedClientLink
                  className="hover:text-ui-fg-base flex gap-2"
                  href="/cart"
                  data-testid="nav-cart-link"
                >
                  Cart(0)
                </LocalizedClientLink>
              }
            >
              <CartButton />
            </Suspense>
          </div>
        </nav>
      </header>
    </div>
  )
}
